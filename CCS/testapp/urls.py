from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    path('home', views.home),
    path('kontakt', views.kontakt),
    path('drogy', views.drogy),
   
]