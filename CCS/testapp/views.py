from django.shortcuts import render
from django.http import HttpResponse

def home(request):
    return render(request, "testapp/home.html")

def kontakt(request):
    return render(request, "testapp/kontakt.html")

def drogy(request):
    return render(request, "testapp/drogy.html")